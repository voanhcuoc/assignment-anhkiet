var bang_danh_bieu = {
    internal_kwd: 'INTERNAL',
    input_kwd: 'INPUT',
    output_kwd: 'OUTPUT',
    add_kwd: 'ADD',
    sub_kwd: 'SUB',
    mul_kwd: 'MUL',
    div_kwd: 'DIV',
    saveto_kwd: 'SAVETO',
    var_name: /[_a-zA-Z][_a-zA-Z0-9]{0,30}/
};

var instruction_spec = {
    internal: {
        grammar: [
            { type: 'internal_kwd' },
            { type: 'var_name', param: 'var' }
        ],
        compile(param) {
            return `int ${param.var};`
        }
    },
    input: {
        grammar: [
            { type: 'input_kwd' },
            { type: 'var_name', param: 'var' }
        ],
        compile(param) {
            return `int ${param.var};\nprintf("Enter ${param.var} = ");  scanf("%d", &${param.var});`
        }
    },
    output: {
        grammar: [
            { type: 'output_kwd' },
            { type: 'var_name', param: 'var' }
        ],
        compile(param) {
            return `printf("Result ${param.var} = %d\\n", ${param.var});`
        }
    },
    add: {
        grammar: [
            { type: 'add_kwd' },
            { type: 'var_name', param: 'a' },
            { type: 'var_name', param: 'b' },
            { type: 'saveto_kwd' },
            { type: 'var_name', param: 'result'}
        ],
        compile(param) {
            return `${param.result} = ${param.a} + ${param.b};`
        }
    },
    sub: {
        grammar: [
            { type: 'sub_kwd' },
            { type: 'var_name', param: 'a' },
            { type: 'var_name', param: 'b' },
            { type: 'saveto_kwd' },
            { type: 'var_name', param: 'result'}
        ],
        compile(param) {
            return `${param.result} = ${param.a} - ${param.b};`
        }
    },
    mul: {
        grammar: [
            { type: 'mul_kwd' },
            { type: 'var_name', param: 'a' },
            { type: 'var_name', param: 'b' },
            { type: 'saveto_kwd' },
            { type: 'var_name', param: 'result'}
        ],
        compile(param) {
            return `${param.result} = ${param.a} * ${param.b};`
        }
    },
    div: {
        grammar: [
            { type: 'div_kwd' },
            { type: 'var_name', param: 'a' },
            { type: 'var_name', param: 'b' },
            { type: 'saveto_kwd' },
            { type: 'var_name', param: 'result'}
        ],
        compile(param) {
            return `${param.result} = ${param.a} / ${param.b};`
        }
    }
}

var leading = '#include <stdio.h>\nint main() {\n';
var trailing = '\nreturn 0;\n};'

document.addEventListener('DOMContentLoaded', init);

function init() {
    document.getElementById('compile').addEventListener('click', compileBtnClick);
}

function compileBtnClick() {
    sourceText = document.getElementById('source').value;
    targetText = compile(sourceText);
    document.getElementById('target').value = targetText;
}

function compile(sourceText) {
    var lines = splitLine(sourceText);
    var lex_result = lines.map(simplelex);
    var meaning_structures = lex_result.map(meaning_analysis)
    var targetCodePortions = meaning_structures.map(build_target_code);
    return leading + targetCodePortions.join('\n') + trailing;
}

function build_target_code(meaning_structure) {
    var inst = meaning_structure.inst;
    var param = meaning_structure.param;
    return inst.compile(param);
}

function parse_token(token) {
    var result = {};
    for (var token_type in bang_danh_bieu) {
        var scenario = bang_danh_bieu[token_type];
        if (
            (token === scenario) ||
            (scenario.test && scenario.test(token))
        ) {
            result = {
                type: token_type,
                value: token
            };
            break;
        }
    };

    return result;
}

// return token list
function simplelex(sourceStr) {
    tokens = sourceStr.split(' ');
    return tokens.map(parse_token);
}

function meaning_analysis(token_list) {
    token_types = token_list.map(token => token.type);
    for (var inst_type in instruction_spec) {
        var inst = instruction_spec[inst_type];
        var scenario_token_types = inst.grammar.map(token => token.type);
        var l = inst.grammar.length;
        if (_.isEqual(token_types, scenario_token_types)) {
            var param = {};
            for (var i=0; i<l; i++) {
                if (inst.grammar[i].param) {
                    param[inst.grammar[i].param] = token_list[i].value;                  
                }
            }
            return {
                inst: inst,
                param: param
            }
        }
    };
}

function splitLine(str) {
    return str.split(/[\r\n]+/);
}

function tap(x) {
    console.log(x);
    return x;
}